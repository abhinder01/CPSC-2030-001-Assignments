// JavaScript source code
var names = new Array('Claude Wallace', 'Riley Miller', 'Raz',
    'Kai Schulen', 'Angelica Farnaby', 'Minerva Victor',
    'Karen Stuart', 'Ragnarok', 'Miles Arbeck', 'Dan Bentley');
var squadArr = new Array();
document.getElementById("bat").innerHTML = "Battalion Roster";
var total = 5;
var data = [
    { name: 'Claude Wallace', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'First Lieutenant', role: 'Tank Commander', disc: 'Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.' }
, { name: 'Riley Miller', side: 'Edinburgh Army', unit: 'Second Lieutenant', role: 'Artillery Advisor', disc: 'Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.' }
, { name: 'Raz', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'Sergeant', role: 'Fireteam Leader', disc: 'Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he is invincible.' }
, { name: 'Kai Schulen', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'First Lieutenant', role: 'Fireteam Leader', disc: '' }
, { name: 'Angelica Farnaby', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'First Lieutenant', role: 'Fireteam Leader', disc: 'A chipper civilian girl who stumbled upon Squad E through strange circumstances. Nicknamed "Angie," she is beloved by the entire squad for her eagerness to help. She seems to be suffering from amnesia, and can only remember her own name.' }
, { name: 'Minerva Victor', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'First Lieutenant', role: 'Senior Commander', disc: 'Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division\'s squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.' }
, { name: 'Karen Stuart', side: 'Edinburgh Army', unit: 'Squad E', rank: 'Corporal', role: 'Combat EMT', disc: 'Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation\'s military to support her growing household.' }
, { name: 'Ragnarok', side: 'Edinburgh Army', unit: 'Squad E', rank: ' K-9 Unit', role: 'Mascot', disc: 'Once a stray, this good good boy is lovingly referred to as "Rags."As a K-9 unit, he\'s a brave and intelligent rescue dog who\'s always willing to lend a helping paw. When the going gets tough, the tough get ruff.' }
, { name: 'Miles Arbeck', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'Sergeant', role: 'Tank Operator', disc: 'Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace\'s partner in tank training, and was delighted to be assigned to Squad E. He\'s taken up photography as a hobby, and is constantly taking snapshots whenever on standby.' }
, { name: 'Dan Bentley', side: 'Edinburgh Army', unit: 'Ranger Corps, Squad E', rank: 'Private First Class', role: 'APC Operator', disc: 'Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.' }
]

for (var i = 0; i < names.length; i++) {
    var div = document.createElement("div");
    div.setAttribute('id', 'name' + (i + 1));
    div.innerHTML = names[i];
    document.getElementById("batRoster").appendChild(div);
}

var i = 0;
let element = document.querySelector("#batRoster");
element.onclick = function (event) {

    var index = 0;
    for (var k = 0; k < names.length; k++) {
        if (names[k] == "" + event.target.firstChild.nodeValue) {
            index = k + 1;
        }
    }
    var img = "img" + index + ".png";
    document.getElementById("pic").src = img;

    var arrind = 0;
    for (var n = 0; n < names.length; n++) {
        if (names[n] == "" + event.target.firstChild.nodeValue) {
            arrind = n;
        }
    }
    document.getElementById("profile").innerHTML = "Name: " + data[arrind].name + "<br>Side: " + data[arrind].side + "<br>Unit: " + data[arrind].unit + "<br>Rank: " + data[arrind].rank + "<br>Role: " + data[arrind].role + "<br>Discription:<br> " + data[arrind].disc;

    if (squadArr.length < total) {
        if (isnotIn(event.target.firstChild.nodeValue)) {
            squadArr.push(event.target.firstChild.nodeValue);
            var div = document.createElement("div");
            div.setAttribute('id', 'sqname' + (i + 1));
            console.log(squadArr.toString());
            div.innerHTML = squadArr[i];
            document.getElementById("squadRoster").appendChild(div);
            i++;
        }
    }
}

function isnotIn(val) {
    for (var j = 0; j < squadArr.length; j++) {
        if (squadArr[j] == val) {
            return false;
        }
    }
    return true;
}
let sq = document.querySelector("#squadRoster");
sq.onclick = function (event) {
    var cha = "" + event.target.id;
    cha = cha.charAt(6);
    cha = parseInt(cha);
    cha = cha - 1;
    squadArr.splice(cha, 1);
    console.log(squadArr.toString());
    var rm = document.getElementById("" + event.target.id);
    rm.parentNode.removeChild(rm);
    i--;
    console.log(total);
    console.log(i);
}