
<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "allcom";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$sql = "SELECT * FROM applications";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
	$pid= array();
	$name = array();
	$add= array();
	$cell= array();
	$dob = array();
	$text= array();
	$email = array();
	$databio= array();
    // output data of each row
    while($row = $result->fetch_assoc()) {
		$pid[] =  $row["PersonID"];
		$name[] =  $row["name"];
		$add[] = $row["Address"];
		$cell[] = $row["contact"];
		$dob[] = $row["dob"];
		$email[] = $row["email"];
		$text[] = $row["textar"];
          }
		 $databio = array($pid ,$name, $add,$cell,$dob,$email,$text) ;
		 echo json_encode($databio);
} else {
    echo "0 results";
}

$conn->close();
?> 