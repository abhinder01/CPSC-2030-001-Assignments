// JavaScript source code
var cart = document.getElementsByClassName('.p1');
var count = 0;
var data, data1;
var dataAppLog;
var dataStckLog;
//animation 
$('.ml3').each(function () {
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({ loop: true })
  .add({
      targets: '.ml3 .letter',
      opacity: [0, 1],
      easing: "easeInOutQuad",
      duration: 2250,
      delay: function (el, i) {
          return 150 * (i + 1)
      }
  }).add({
      targets: '.ml3',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
  });
//add items to cart
function addCart(event) {
    count++;
    $('.p1').attr({ "data-count": count });
    var val = $(event.target).attr("value");
    if (val == 1) {
        var srno = $('#srnoid' + val).attr("srno");
        var name = $('#nameid' + val).attr("name");
        var ratings = $('#ratingid' + val).attr("ratings");
        var price = $('#priceid' + val).attr("price");
        var dataString = 'srnos=' + srno + '&names=' + name + '&ratin=' + ratings + '&prices=' + price;
    }
    else if (val == 2) {
        var srno = $('#srnoid' + val).attr("srno");
        var name = $('#nameid' + val).attr("name");
        var ratings = $('#ratingid' + val).attr("ratings");
        var price = $('#priceid' + val).attr("price");
        var dataString = 'srnos=' + srno + '&names=' + name + '&ratin=' + ratings + '&prices=' + price;
    }
    else if (val == 3) {
        var srno = $('#srnoid' + val).attr("srno");
        var name = $('#nameid' + val).attr("name");
        var ratings = $('#ratingid' + val).attr("ratings");
        var price = $('#priceid' + val).attr("price");
        var dataString = 'srnos=' + srno + '&names=' + name + '&ratin=' + ratings + '&prices=' + price;
    }
    else if (val == 4) {
        var srno = $('#srnoid' + val).attr("srno");
        var name = $('#nameid' + val).attr("name");
        var ratings = $('#ratingid' + val).attr("ratings");
        var price = $('#priceid' + val).attr("price");
        var dataString = 'srnos=' + srno + '&names=' + name + '&ratin=' + ratings + '&prices=' + price;
    }
    $.ajax({
        url: "cart.php",
        method: "POST",
        data: dataString,
        cache: false,
        success: function (rslt) {
            console.log("done" + rslt);
        }
    })
}
function setup() {
    $.ajax({
        url: "mycart.php",
        method: "POST",
        cache: false,
        success: function (rslt) {
            if (rslt != 0) {
                data = JSON.parse(rslt);
                for (var i = 0; i < data[0].length; i++) {
                    var chdiv = document.createElement("DIV");
                    chdiv.setAttribute("class", "srvice");
                    document.getElementById("servicediv").appendChild(chdiv);
                    var img = document.createElement("IMG");
                    if (data[1][i] == "Cabels") {
                        img.setAttribute("src", "cable.jpg");
                    }
                    else if (data[1][i] == "Port_Cabels") {
                        img.setAttribute("src", "portcables.jpg");
                    }
                    else if (data[1][i] == "Handsets") {
                        img.setAttribute("src", "handset.jpg");
                    }
                    else if (data[1][i] == "Shoulder Reset") {
                        img.setAttribute("src", "shoulder.jpg");
                    }
                    document.getElementsByClassName("srvice")[i].appendChild(img);
                    var zs = document.createElement("P");
                    var ts = document.createTextNode("Serial Number: " + data[0][i]);
                    zs.appendChild(ts);
                    document.getElementsByClassName("srvice")[i].appendChild(zs);
                    var zn = document.createElement("P");
                    var tn = document.createTextNode("Name: " + data[1][i]);
                    zn.appendChild(tn);
                    document.getElementsByClassName("srvice")[i].appendChild(zn);
                    var zr = document.createElement("P");
                    var tr = document.createTextNode("Ratings: " + data[2][i]);
                    zr.appendChild(tr);
                    document.getElementsByClassName("srvice")[i].appendChild(zr);
                    var zp = document.createElement("P");
                    var tp = document.createTextNode("Price: " + data[3][i]);
                    zp.appendChild(tp);
                    document.getElementsByClassName("srvice")[i].appendChild(zp);
                  
                }

            }
    else{
                alert("Your Cart is empty");
        }
    }
    })
}
$(".ordrnow").click(function (event) {
    window.open("orderItem.php");
});
function myFun(event) {
    $.ajax({
        url: "mycartrm.php",
        method: "POST",
        cache: false,
        success: function (rslt) {
            console.log(rslt);
            location.reload();
        }
    })
};
var modal = document.getElementById('myModal');
var span = document.getElementsByClassName("close")[0];
setTimeout(function () { $(".modal").css("display", "block"); }, 5000);
span.onclick = function () {
    $(".modal").css("display", "none");
}
var modals = document.getElementsByClassName('myModals')[0];
var spans = document.getElementsByClassName("closes")[0];
var btn = document.getElementsByClassName("p4")[0];
btn.onclick = function () {
    $(".modals").css("display", "block");
}
spans.onclick = function () {
    $(".modals").css("display", "none");
}
$(".logbttn").click(function (event) {
    $user = $("#usernames").val();
    $pass = $("#pass").val();
    if ($user == "cpsc2030" && $pass == "1234") {
        window.open("login.html");
    }
    else {
        alert("Wrong Username or password");
    }
});

function setupAppLog() {
    $.ajax({
        url: "applog.php",
        method: "POST",
        cache: false,
        success: function (rslt) {
            dataAppLog = JSON.parse(rslt);
            for (var i = 0; i < dataAppLog[0].length; i++) {
                var chdiv = document.createElement("DIV");
                chdiv.setAttribute("class", "mySlides");
                document.getElementById("mainAppLog").appendChild(chdiv);
                var zs = document.createElement("P");
                var ts = document.createTextNode("Person Id: " + dataAppLog[0][i]);
                zs.appendChild(ts);
                document.getElementsByClassName("mySlides")[i].appendChild(zs);
                var zn = document.createElement("P");
                var tn = document.createTextNode("Name: " + dataAppLog[1][i]);
                zn.appendChild(tn);
                document.getElementsByClassName("mySlides")[i].appendChild(zn);
                var zr = document.createElement("P");
                var tr = document.createTextNode("Address: " + dataAppLog[2][i]);
                zr.appendChild(tr);
                document.getElementsByClassName("mySlides")[i].appendChild(zr);
                var zp = document.createElement("P");
                var tp = document.createTextNode("Contact: " + dataAppLog[3][i]);
                zp.appendChild(tp);
                document.getElementsByClassName("mySlides")[i].appendChild(zp);
                var zd = document.createElement("P");
                var td = document.createTextNode("Date Of Birth: " + dataAppLog[4][i]);
                zd.appendChild(td);
                document.getElementsByClassName("mySlides")[i].appendChild(zd);
                var ze = document.createElement("P");
                var te = document.createTextNode("Email: " + dataAppLog[5][i]);
                ze.appendChild(te);
                document.getElementsByClassName("mySlides")[i].appendChild(ze);
                var zt = document.createElement("P");
                var tt = document.createTextNode("Skills: " + dataAppLog[6][i]);
                zt.appendChild(tt);
                document.getElementsByClassName("mySlides")[i].appendChild(zt);
            }

        }
    })
}
function setupStckLog() {
    $.ajax({
        url: "stckLog.php",
        method: "POST",
        cache: false,
        success: function (rslt) {
            dataStckLog = JSON.parse(rslt);
            for (var i = 0; i < dataStckLog[0].length; i++) {
                var chdiv = document.createElement("DIV");
                chdiv.setAttribute("class", "mySlides");
                document.getElementById("mainAppLog").appendChild(chdiv);
                var zs = document.createElement("P");
                var ts = document.createTextNode("Name: " + dataStckLog[0][i] + " " + dataStckLog[1][i]);
                zs.appendChild(ts);
                document.getElementsByClassName("mySlides")[i].appendChild(zs);
                var zr = document.createElement("P");
                var tr = document.createTextNode("Address: " + dataStckLog[2][i] + " " + dataStckLog[3][i]);
                zr.appendChild(tr);
                document.getElementsByClassName("mySlides")[i].appendChild(zr);
                var zd = document.createElement("P");
                var td = document.createTextNode("Items: " + dataStckLog[4][i]);
                zd.appendChild(td);
                document.getElementsByClassName("mySlides")[i].appendChild(zd);
                var ze = document.createElement("P");
                var te = document.createTextNode("Email: " + dataStckLog[5][i]);
                ze.appendChild(te);
                document.getElementsByClassName("mySlides")[i].appendChild(ze);
                var zt = document.createElement("P");
                var tt = document.createTextNode("Price: " + dataStckLog[6][i]);
                zt.appendChild(tt);
                document.getElementsByClassName("mySlides")[i].appendChild(zt);
            }

        }
    })
}
$(".submitForm").click(function (event) {
    $fname = $("#fname").val();
    $lname = $("#lname").val();
    $address = $("#address").val();
    $email = $("#email").val();
    $country = $("#country").val();
    $orderstck = $("#orderstck").val();
    $price = $("#price").val();
    var dataString = 'fname=' + $fname + '&lname=' + $lname + '&address=' + $address + '&email=' + $email + '&country=' + $country + '&orderstck=' + $orderstck + '&price=' + $price;
    $.ajax({
        url: "order.php",
        method: "POST",
        data: dataString,
        cache: false,
        success: function (rslt) {
            window.open("home.html");
        }
    })
});
