<?php
include 'data.php';
   require_once './vendor/autoload.php';  //include the twig library.
   $loader = new Twig_Loader_Filesystem('./templates');
    $twig = new Twig_Environment($loader);
   $price = getPrice();
   $sum = array_sum($price);
   $input = implode(getName());
      $template = $twig->load('order.twig.html');
	  echo $template->render(array("Names"=>getName(),"Price"=>$sum , "NameStr"=>$input));
?>