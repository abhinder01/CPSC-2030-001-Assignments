﻿
<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "allcom";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$sql = "SELECT * FROM order_stock";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
	$fname= array();
	$lname = array();
	$add= array();
	$country= array();
	$price = array();
    $items = array();
	$email = array();
	$databio= array();
    // output data of each row
    while($row = $result->fetch_assoc()) {
		$fname[] =  $row["FirstName"];
		$lname[] =  $row["LastName"];
		$add[] = $row["Address"];
		$country[] = $row["Country"];
        $items[] = $row["Item"];
		$price[] = $row["price"];
		$email[] = $row["email"];
          }
		 $databio = array($fname ,$lname, $add,$country,$items,$email,$price) ;
		 echo json_encode($databio);
} else {
    echo "0 results";
}

$conn->close();
?> 