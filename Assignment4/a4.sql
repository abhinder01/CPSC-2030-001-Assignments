CREATE TABLE Pokemon(
   Nat    INTEGER  NOT NULL PRIMARY KEY 
  ,Hoenn  VARCHAR(3)
  ,Name   VARCHAR(21) NOT NULL
  ,Type   VARCHAR(12)
  ,HP     INTEGER 
  ,Atk    INTEGER 
  ,Def    INTEGER 
  ,SAt    INTEGER 
  ,SDf    INTEGER 
  ,Spd    INTEGER 
  ,BST    INTEGER 
);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (1,'-','Bulbasaur','GRASSPOISON',45,49,49,65,65,45,318);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (2,'-','Ivysaur','GRASSPOISON',60,62,63,80,80,60,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (3,'-','Venusaur','GRASSPOISON',80,82,83,100,100,80,525);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (3,'-','Mega Venusaur','GRASSPOISON',80,100,123,122,120,80,625);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (4,'-','Charmander','FIRE',39,52,43,60,50,65,309);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (5,'-','Charmeleon','FIRE',58,64,58,80,65,80,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (6,'-','Charizard','FIREFLYING',78,84,78,109,85,100,534);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (6,'-','Mega Charizard X','FIREDRAGON',78,120,78,100,85,100,561);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (6,'-','Mega Charizard Y','FIREFLYING',78,104,78,159,115,100,634);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (7,'-','Squirtle','WATER',44,48,65,50,64,43,314);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (8,'-','Wartortle','WATER',59,63,80,65,80,58,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (9,'-','Blastoise','WATER',79,83,100,85,105,78,530);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (9,'-','Mega Blastoise','WATER',79,83,100,85,105,78,530);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (10,'-','Caterpie','BUG',45,30,35,20,20,45,195);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (11,'-','Metapod','BUG',50,20,55,25,25,30,205);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (12,'-','Butterfree','BUGFLYING',60,45,50,90,80,70,395);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (13,'-','Weedle','BUGPOISON',40,35,30,20,20,50,195);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (14,'-','Kakuna','BUGPOISON',45,25,50,25,25,35,205);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (15,'-','Beedrill','BUGPOISON',65,90,40,45,80,75,395);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (15,'-','Mega Beedrill','BUGPOISON',65,150,40,15,80,145,495);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (16,'-','Pidgey','NORMALFLYING',40,45,40,35,35,56,251);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (17,'-','Pidgeotto','NORMALFLYING',63,60,55,50,50,71,349);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (18,'-','Pidgeot','NORMALFLYING',83,80,75,70,70,101,479);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (18,'-','Mega Pidgeot','NORMALFLYING',83,80,80,135,80,121,579);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (19,'-','Rattata','NORMAL',30,56,35,25,35,72,253);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (20,'-','Raticate','NORMAL',55,81,60,50,70,97,413);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (21,'-','Spearow','NORMALFLYING',40,60,30,31,31,70,262);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (22,'-','Fearow','NORMALFLYING',65,90,65,61,61,100,442);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (23,'-','Ekans','POISON',35,60,44,40,54,55,288);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (24,'-','Arbok','POISON',60,85,69,65,79,80,438);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (25,'163','Pikachu','ELECTR',35,55,40,50,50,90,320);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (25,'163','Pikachu','ELECTR',35,55,40,50,50,90,320);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (NULL,NULL,'(Pikachu Rock Star)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (NULL,NULL,'(Cosplay Pikachu)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (26,'164','Raichu','ELECTR',60,90,55,90,80,110,485);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (27,'117','Sandshrew','GROUND',50,75,85,20,30,40,300);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (28,'118','Sandslash','GROUND',75,100,110,45,55,65,450);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (29,'-','Nidoran♀','POISON',55,47,52,40,40,41,275);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (30,'-','Nidorina','POISON',70,62,67,55,55,56,365);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (31,'-','Nidoqueen','POISONGROUND',90,92,87,75,85,76,505);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (32,'-','Nidoran♂','POISON',46,57,40,40,40,50,273);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (33,'-','Nidorino','POISON',61,72,57,55,55,65,365);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (34,'-','Nidoking','POISONGROUND',81,102,77,85,75,85,505);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (35,'-','Clefairy','FAIRY',70,45,48,60,65,35,323);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (36,'-','Clefable','FAIRY',95,70,73,95,90,60,483);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (37,'160','Vulpix','FIRE',38,41,40,50,65,65,299);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (38,'161','Ninetales','FIRE',73,76,75,81,100,100,505);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (39,'143','Jigglypuff','NORMALFAIRY',115,45,20,45,25,20,270);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (40,'144','Wigglytuff','NORMALFAIRY',140,70,45,85,50,45,435);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (41,'65','Zubat','POISONFLYING',40,45,35,30,40,55,245);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (42,'66','Golbat','POISONFLYING',75,80,70,65,75,90,455);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (43,'91','Oddish','GRASSPOISON',45,50,55,75,65,30,320);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (44,'92','Gloom','GRASSPOISON',60,65,70,85,75,40,395);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (45,'93','Vileplume','GRASSPOISON',75,80,85,110,90,50,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (46,'-','Paras','BUGGRASS',35,70,55,45,55,25,285);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (47,'-','Parasect','BUGGRASS',60,95,80,60,80,30,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (48,'-','Venonat','BUGPOISON',60,55,50,40,55,45,305);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (49,'-','Venomoth','BUGPOISON',70,65,60,90,75,90,450);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (50,'-','Diglett','GROUND',10,55,25,35,45,95,265);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (51,'-','Dugtrio','GROUND',35,80,50,50,70,120,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (52,'-','Meowth','NORMAL',40,45,35,40,40,90,290);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (53,'-','Persian','NORMAL',65,70,60,65,65,115,440);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (54,'165','Psyduck','WATER',50,52,48,65,50,55,320);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (55,'166','Golduck','WATER',80,82,78,95,80,85,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (56,'-','Mankey','FIGHT',40,80,35,35,45,70,305);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (57,'-','Primeape','FIGHT',65,105,60,60,70,95,455);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (58,'-','Growlithe','FIRE',55,70,45,70,50,60,350);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (59,'-','Arcanine','FIRE',90,110,80,100,80,95,555);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (60,'-','Poliwag','WATER',40,50,40,40,40,90,300);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (61,'-','Poliwhirl','WATER',65,65,65,50,50,90,385);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (62,'-','Poliwrath','WATERFIGHT',90,95,95,70,90,70,510);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (63,'40','Abra','PSYCHC',25,20,15,105,55,90,310);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (64,'41','Kadabra','PSYCHC',40,35,30,120,70,105,400);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (65,'42','Alakazam','PSYCHC',55,50,45,135,95,120,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (65,'42','Mega Alakazam','PSYCHC',55,50,65,175,95,150,590);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (66,'75','Machop','FIGHT',70,80,50,35,35,35,305);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (67,'76','Machoke','FIGHT',80,100,70,50,60,45,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (68,'77','Machamp','FIGHT',90,130,80,65,85,55,505);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (69,'-','Bellsprout','GRASSPOISON',50,75,35,70,30,40,300);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (70,'-','Weepinbell','GRASSPOISON',65,90,50,85,45,55,390);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (71,'-','Victreebel','GRASSPOISON',80,105,65,100,70,70,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (72,'68','Tentacool','WATERPOISON',40,40,35,50,100,70,335);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (73,'69','Tentacruel','WATERPOISON',80,70,65,80,120,100,515);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (74,'58','Geodude','ROCKGROUND',40,80,100,30,30,20,300);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (75,'59','Graveler','ROCKGROUND',55,95,115,45,45,35,390);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (76,'60','Golem','ROCKGROUND',80,120,130,55,65,45,495);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (77,'-','Ponyta','FIRE',50,85,55,65,65,90,410);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (78,'-','Rapidash','FIRE',65,100,70,80,80,105,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (79,'-','Slowpoke','WATERPSYCHC',90,65,65,40,40,15,315);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (80,'-','Slowbro','WATERPSYCHC',95,75,110,100,80,30,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (80,'-','Mega Slowbro','WATERPSYCHC',95,75,180,130,80,30,590);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (81,'84','Magnemite','ELECTRSTEEL',25,35,70,95,55,45,325);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (82,'85','Magneton','ELECTRSTEEL',50,60,95,120,70,70,465);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (83,'-','Farfetch’d','NORMALFLYING',52,65,55,58,62,60,352);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (84,'95','Doduo','NORMALFLYING',35,85,45,35,35,75,310);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (85,'96','Dodrio','NORMALFLYING',60,110,70,60,60,100,460);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (86,'-','Seel','WATER',65,45,55,45,70,45,325);

INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (87,'-','Dewgong','WATERICE',90,70,80,70,95,70,475);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (88,'111','Grimer','POISON',80,80,50,40,50,25,325);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (89,'112','Muk','POISON',105,105,75,65,100,50,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (90,'-','Shellder','WATER',30,65,100,45,25,40,305);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (91,'-','Cloyster','WATERICE',50,95,180,85,45,70,525);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (92,'-','Gastly','GHOSTPOISON',30,35,30,100,35,80,310);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (93,'-','Haunter','GHOSTPOISON',45,50,45,115,55,95,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (94,'-','Gengar','GHOSTPOISON',60,65,60,130,75,110,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (94,'-','Mega Gengar','GHOSTPOISON',60,65,80,170,95,130,600);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (95,'-','Onix','ROCKGROUND',35,45,160,30,45,70,385);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (96,'-','Drowzee','PSYCHC',60,48,45,43,90,42,328);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (97,'-','Hypno','PSYCHC',85,73,70,73,115,67,483);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (98,'-','Krabby','WATER',30,105,90,25,25,50,325);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (99,'-','Kingler','WATER',55,130,115,50,50,75,475);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (100,'87','Voltorb','ELECTR',40,30,50,55,55,100,330);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (101,'88','Electrode','ELECTR',60,50,70,80,80,140,480);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (102,'-','Exeggcute','GRASSPSYCHC',60,40,80,60,45,40,325);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (103,'-','Exeggutor','GRASSPSYCHC',95,95,85,125,65,55,520);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (104,'-','Cubone','GROUND',50,50,95,40,50,35,320);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (105,'-','Marowak','GROUND',60,80,110,50,80,45,425);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (106,'-','Hitmonlee','FIGHT',50,120,53,35,110,87,455);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (107,'-','Hitmonchan','FIGHT',50,105,79,35,110,76,455);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (108,'-','Lickitung','NORMAL',90,55,75,60,75,30,385);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (109,'113','Koffing','POISON',40,65,95,60,45,35,340);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (110,'114','Weezing','POISON',65,90,120,85,70,60,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (111,'176','Rhyhorn','GROUNDROCK',80,85,95,30,30,25,345);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (112,'177','Rhydon','GROUNDROCK',105,130,120,45,45,40,485);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (113,'-','Chansey','NORMAL',250,5,5,35,105,50,450);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (114,'-','Tangela','GRASS',65,55,115,100,40,60,435);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (115,'-','Kangaskhan','NORMAL',105,95,80,40,80,90,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (115,'-','Mega Kangaskhan','NORMAL',105,125,100,60,100,100,590);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (116,'193','Horsea','WATER',30,40,70,70,25,60,295);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (117,'194','Seadra','WATER',55,65,95,95,45,85,440);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (119,'52','Seaking','WATER',80,92,65,65,80,68,450);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (120,'148','Staryu','WATER',30,45,55,70,55,85,340);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (121,'149','Starmie','WATERPSYCHC',60,75,85,100,85,115,520);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (122,'-','Mr. Mime','PSYCHCFAIRY',40,45,65,100,120,90,460);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (123,'-','Scyther','BUGFLYING',70,110,80,55,80,105,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (124,'-','Jynx','ICEPSYCHC',65,50,35,115,95,95,455);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (125,'-','Electabuzz','ELECTR',65,83,57,95,85,105,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (126,'-','Magmar','FIRE',65,95,57,100,85,93,495);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (127,'174','Pinsir','BUG',65,125,100,55,70,85,500);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (127,'174','Mega Pinsir','BUGFLYING',65,155,120,65,90,105,600);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (128,'-','Tauros','NORMAL',75,100,95,40,70,110,490);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (129,'53','Magikarp','WATER',20,10,55,15,20,80,200);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (130,'54','Gyarados','WATERFLYING',95,125,79,60,100,81,540);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (130,'54','Mega Gyarados','WATERDARK',95,155,109,70,130,81,640);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (131,'-','Lapras','WATERICE',130,85,80,85,95,60,535);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (132,'-','Ditto','NORMAL',48,48,48,48,48,48,288);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (133,'-','Eevee','NORMAL',55,55,50,45,65,55,325);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (134,'-','Vaporeon','WATER',130,65,60,110,95,65,525);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (135,'-','Jolteon','ELECTR',65,65,60,110,95,130,525);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (136,'-','Flareon','FIRE',65,130,60,95,110,65,525);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (137,'-','Porygon','NORMAL',65,60,70,85,75,40,395);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (138,'-','Omanyte','ROCKWATER',35,40,100,90,55,35,355);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (139,'-','Omastar','ROCKWATER',70,60,125,115,70,55,495);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (140,'-','Kabuto','ROCKWATER',30,80,90,55,45,55,355);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (141,'-','Kabutops','ROCKWATER',60,115,105,65,70,80,495);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (142,'-','Aerodactyl','ROCKFLYING',80,105,65,60,75,130,515);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (142,'-','Mega Aerodactyl','ROCKFLYING',80,135,85,70,95,150,615);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (143,'-','Snorlax','NORMAL',160,110,65,65,110,30,540);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (144,'-','Articuno','ICEFLYING',90,85,100,95,125,85,580);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (145,'-','Zapdos','ELECTRFLYING',90,90,85,125,90,100,580);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (146,'-','Moltres','FIREFLYING',90,100,90,125,85,90,580);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (147,'-','Dratini','DRAGON',41,64,45,50,50,50,300);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (148,'-','Dragonair','DRAGON',61,84,65,70,70,70,420);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (149,'-','Dragonite','DRAGONFLYING',91,134,95,100,100,80,600);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (150,'-','Mewtwo','PSYCHC',106,110,90,154,90,130,680);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (150,'-','Mega Mewtwo X','PSYCHCFIGHT',106,190,100,154,100,130,780);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (150,'-','Mega Mewtwo Y','PSYCHC',106,150,70,194,120,140,780);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (151,'-','Mew','PSYCHC',100,100,100,100,100,100,600);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (152,'-','Chikorita','GRASS',45,49,65,49,65,45,318);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (153,'-','Bayleef','GRASS',60,62,80,63,80,60,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (154,'-','Meganium','GRASS',80,82,100,83,100,80,525);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (155,'-','Cyndaquil','FIRE',39,52,43,60,50,65,309);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (156,'-','Quilava','FIRE',58,64,58,80,65,80,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (157,'-','Typhlosion','FIRE',78,84,78,109,85,100,534);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (158,'-','Totodile','WATER',50,65,64,44,48,43,314);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (159,'-','Croconaw','WATER',65,80,80,59,63,58,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (160,'-','Feraligatr','WATER',85,105,100,79,83,78,530);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (161,'-','Sentret','NORMAL',35,46,34,35,45,20,215);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (162,'-','Furret','NORMAL',85,76,64,45,55,90,415);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (163,'-','Hoothoot','NORMALFLYING',60,30,30,36,56,50,262);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (164,'-','Noctowl','NORMALFLYING',100,50,50,76,96,70,442);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (165,'-','Ledyba','BUGFLYING',40,20,30,40,80,55,265);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (166,'-','Ledian','BUGFLYING',55,35,50,55,110,85,390);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (167,'-','Spinarak','BUGPOISON',40,60,40,40,40,30,250);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (168,'-','Ariados','BUGPOISON',70,90,70,60,60,40,390);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (169,'67','Crobat','POISONFLYING',85,90,80,70,80,130,535);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (170,'190','Chinchou','WATERELECTR',75,38,38,56,56,67,330);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (171,'191','Lanturn','WATERELECTR',125,58,58,76,76,67,460);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (172,'162','Pichu','ELECTR',20,40,15,35,35,60,205);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (173,'-','Cleffa','FAIRY',50,25,28,45,55,15,218);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (174,'142','Igglybuff','NORMALFAIRY',90,30,15,40,20,15,210);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (175,'-','Togepi','FAIRY',35,20,65,40,65,20,245);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (176,'-','Togetic','FAIRYFLYING',55,40,85,80,105,40,405);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (177,'169','Natu','PSYCHCFLYING',40,50,45,70,45,70,320);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (178,'170','Xatu','PSYCHCFLYING',65,75,70,95,70,95,470);
INSERT INTO Pokemon(Nat,Hoenn,Name,Type,HP,Atk,Def,SAt,SDf,Spd,BST) VALUES (179,'-','Mareep','ELECTR',55,40,40,65,45,35,280);









CREATE TABLE Type(
   Type           VARCHAR (8)  NOT NULL PRIMARY KEY
  ,Strong_Against VARCHAR (35)
  ,Weak_Against   VARCHAR (51) NOT NULL
  ,Resistant_To   VARCHAR (76) NOT NULL
  ,Vulnerable_To  VARCHAR (37) NOT NULL
);
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Normal',NULL,'Rock, Ghost, Steel','Ghost','Fighting');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Fighting','Normal, Rock, Steel, Ice, Dark','Flying, Poison, Psychic, Bug, Ghost, Fairy','Rock, Bug, Dark','Flying, Psychic, Fairy');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Flying','Fighting, Bug, Grass','Rock, Steel, Electric','Fighting, Ground, Bug, Grass','Rock, Electric, Ice');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Poison','Grass, Fairy','Poison, Ground, Rock, Ghost, Steel','Fighting, Poison, Grass, Fairy','Ground, Psychic');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Ground','Poison, Rock, Steel, Fire, Electric','Flying, Bug, Grass','Poison, Rock, Electric','Water, Grass, Ice');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Rock','Flying, Bug, Fire, Ice','Fighting, Ground, Steel','Normal, Flying, Poison, Fire','Fighting, Ground, Steel, Water, Grass');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Bug','Grass, Psychic, Dark','Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy','Fighting, Ground, Grass','Flying, Rock, Fire');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Ghost','Ghost, Psychic','Normal, Dark','Normal, Fighting, Poison, Bug','Ghost, Dark');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Steel','Rock, Ice, Fairy','Steel, Fire, Water, Electric','Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy','Fighting, Ground, Fire');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Fire','Bug, Steel, Grass, Ice','Rock, Fire, Water, Dragon','Bug, Steel, Fire, Grass, Ice','Ground, Rock, Water');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Water','Ground, Rock, Fire','Water, Grass, Dragon','Steel, Fire, Water, Ice','Grass, Electric');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Grass','Ground, Rock, Water','Flying, Poison, Bug, Steel, Fire, Grass, Dragon','Ground, Water, Grass, Electric','Flying, Poison, Bug, Fire, Ice');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Electric','Flying, Water','Ground, Grass, Electric, Dragon','Flying, Steel, Electric','Ground');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Psychic','Fighting, Poison','Steel, Psychic, Dark','Fighting, Psychic','Bug, Ghost, Dark');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Ice','Flying, Ground, Grass, Dragon','Steel, Fire, Water, Ice','Ice','Fighting, Rock, Steel, Fire');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Dragon','Dragon','Steel, Fairy','Fire, Water, Grass, Electric','Ice, Dragon, Fairy');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Fairy','Fighting, Dragon, Dark','Poison, Steel, Fire','Fighting, Bug, Dragon, Dark','Poison, Steel');
INSERT INTO Type(Type,Strong_Against,Weak_Against,Resistant_To,Vulnerable_To) VALUES ('Dark','Ghost, Psychic','Fighting, Dark, Fairy','Ghost, Psychic, Dark','Fighting, Bug, Fairy');
